//
//  ReversiAI.swift
//  Reversi
//
import UIKit

class ReversiAI
{
    var logic : ReversiLogic!
    var controller : ViewController!
    
    init(logic: ReversiLogic, controller: ViewController) {
        self.logic = logic
        self.controller = controller
    }
    
    func calculateMove() -> UIButton {
        var max_score = 0
        let availableCells = logic.getAvailableCells()
        var x = -1
        var y = -1
        var field = copyField(original: logic.getField())
        for cell in availableCells {
            if x == -1 && y == -1 {
                x = cell.x
                y = cell.y
            }
            let place = Board(tag: cell.toTag())
            let previousState = field[cell.x][cell.y]
            if logic.canStepOnCopy(at: place, available: availableCells) {
                field[cell.x][cell.y] = State.Player_B
                let score = logic.tryCaptureCells(start: Board(tag: cell.toTag()), fieldCopy: field, player: field[cell.x][cell.y])
                if max_score < score  {
                    max_score = score
                    x = cell.x
                    y = cell.y
                }
            }
            field[cell.x][cell.y] = previousState
        }
        return controller.getCurrentButton(x: x, y: y)
    }
    
    func calculateMoveRecursively(originalField: [[State]], counter: Int) -> Int {
        var max_score = 0
        let currentPlayer = counter % 2 == 0 ? State.Player_B : State.Player_A
        let availableCells = logic.getAvailableCells(field: originalField, player: currentPlayer)
        var x = -1
        var y = -1
        var field = copyField(original: originalField)
        
        for cell in availableCells {
            if x == -1 && y == -1 {
                x = cell.x
                y = cell.y
            }
            let place = Board(tag: cell.toTag())
            let previousState = field[cell.x][cell.y]
            if logic.canStepOnCopy(at: place, available: availableCells) {
                field[cell.x][cell.y] = currentPlayer
                
                let score = logic.tryCaptureCells(start: Board(tag: cell.toTag()), fieldCopy: field, player: field[cell.x][cell.y])
                if max_score < score  {
                    max_score = score
                    x = cell.x
                    y = cell.y
                }
            }
            field[cell.x][cell.y] = previousState
        }
        return max_score
    }
    
    func copyField(original: [[State]]) -> [[State]] {
        //Copy current state of field
        var field = [[State]](repeating:[State](repeating:State.None, count: logic.getField()[0].count), count:logic.getField().count)
        for i in 0..<logic.getField().count {
            for j in 0..<logic.getField()[i].count {
                field[i][j] = logic.getField()[i][j]
            }
        }
        return field
    }
    
    func makeMove() {
        controller.disableButtons()
        controller.showOverlay(controller.view)
        DispatchQueue.global(qos: .background).async {
            if self.logic.getAvailableCells().count > 0 {
                if self.logic.getCurrentPlayer() == .Player_B {
                }
            }
            DispatchQueue.main.async {
                self.controller.clickButton(self.calculateMove())
                self.controller.hideOverlayView()
                self.controller.enableButtons()
            }
        }
    }
}
