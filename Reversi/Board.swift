//
//  Board.swift
//  Reversi
//
struct Board
{
    var x: Int
    var y: Int
    
    func toTag() -> Int {
        return x * 100 + y
    }
    
    init(tag: Int) {
        x = tag / 100
        y = tag % 100
    }
    
    init(x: Int, y: Int) {
        self.x = x
        self.y = y
    }
    
    static var offsets: [Board] = [
        Board(x:  0, y:  1),
        Board(x:  0, y: -1),
        Board(x:  1, y:  0),
        Board(x: -1, y:  0),
        
        Board(x:  1, y:  1),
        Board(x: -1, y: -1),
        Board(x:  1, y: -1),
        Board(x: -1, y:  1),
    ]
}

extension Board {
    static func +(left: Board, right: Board) -> Board {
        return Board(x: left.x + right.x, y: left.y + right.y)
    }
    static func -(left: Board, right: Board) -> Board {
        return Board(x: left.x - right.x, y: left.y - right.y)
    }
}

extension Board : Hashable {
    var hashValue: Int {
        return toTag()
    }
    
    static func ==(lhs: Board, rhs: Board) -> Bool {
        return lhs.x == rhs.x && lhs.y == rhs.y;
    }
}
