//
//  State.swift
//  Reversi
//
enum State
{
    case None
    case Player_A
    case Player_B
}
