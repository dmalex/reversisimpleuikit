//
//  ViewController.swift
//  Reversi
//
import UIKit

class ViewController: UIViewController
{
    @IBOutlet weak var container: UIView!
    var current_position : [[UIButton]] = []
    var fieldWidth = 8
    var fieldHeight = 8
    var logic : ReversiLogic!
    var ai : ReversiAI!
    
    @IBOutlet weak var currentPlayerLabel: UILabel!
    
    func setAvailableButtonColors(color: UIColor) {
        for pos in logic.availableCells {
            current_position[pos.x][pos.y].backgroundColor = color
        }
    }
    
    func setColors(available: UIColor, none: UIColor) {
        for x in 0..<current_position.count {
            for y in 0..<current_position[0].count {
                if logic.availableCells.contains(where: {$0.x == x && $0.y == y}) {
                    current_position[x][y].backgroundColor = available
                }
            }
        }
        for x in 0..<logic.getField().count {
            for y in 0..<logic.getField()[0].count {
                if logic.getField()[x][y] == State.None && !logic.availableCells.contains(where: {$0.x == x && $0.y == y}){
                    current_position[x][y].backgroundColor = none
                }
            }
        }
    }
    
    func getCurrentButton(x: Int, y: Int) -> UIButton {
        return current_position[x][y]
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        logic = ReversiLogic(width: fieldWidth, height: fieldHeight)
        currentPlayerLabel.backgroundColor = UIColor.yellow
        generateButtonField(x: fieldWidth, y: fieldHeight, screenWidth: container.frame.width, screenHeight:  container.frame.height)
        ai = ReversiAI(logic: logic, controller: self)
    }
    
    func generateButtonField(x: Int, y: Int, screenWidth: CGFloat, screenHeight: CGFloat) {
        let maxWidth  = screenWidth  / CGFloat(x)
        let maxHeight = screenHeight / CGFloat(y)
        let buttonSize = min(maxWidth, maxHeight)
        
        current_position = (0..<y).map {
            ix in (0..<x).map {
                iy in {
                    let position = UIButton(type: .custom)
                    container.addSubview(position)
                    position.tag = 100 * ix + iy
                    switch logic.fieldAt(Board(tag: position.tag)) {
                    case .None:
                        position.backgroundColor = UIColor.white
                    case .Player_A:
                        position.backgroundColor = UIColor.yellow
                    case .Player_B:
                        position.backgroundColor = UIColor.red
                    }
                    position.layer.borderWidth = 1
                    position.layer.cornerRadius = 1
                    position.layer.borderColor = UIColor.black.cgColor
                    position.frame = CGRect(x: CGFloat(ix) * buttonSize, y: CGFloat(iy) * buttonSize, width: buttonSize, height: buttonSize)
                    position.addTarget(self, action: #selector(ViewController.onButtonTouch(_:)), for: .touchUpInside)
                    
                    position.addTarget(self, action: #selector(ViewController.makeMove(_:)), for: .touchUpInside)
                    return position
                }()
            }
        }
        setColors(available: UIColor.lightGray, none: UIColor.white)
    }
    
    @objc func makeMove(_ sender: UIButton) {
        if logic.getCurrentPlayer() == .Player_B {
            ai.makeMove()
        }
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        clearButtons()
        generateButtonField(x: fieldWidth, y: fieldHeight, screenWidth: container.frame.width, screenHeight:  container.frame.height)
    }
    
    @objc func onButtonTouch(_ sender: UIButton) {
        disableButtons()
        let place = Board(tag: sender.tag)
        if logic.canStep(at: place) {
            setColors(available: UIColor.darkGray, none: UIColor.white)
            switch logic.onStep(at: place) {
            case .Player_A:
                sender.backgroundColor = UIColor.yellow
            case .Player_B:
                sender.backgroundColor = UIColor.red
            default:
                break
            }
            for pos in logic.capturedCells {
                switch logic.fieldAt(pos) {
                case .None:
                    break;
                case .Player_A:
                    current_position[pos.x][pos.y].backgroundColor = UIColor.yellow
                    break;
                case .Player_B:
                    current_position[pos.x][pos.y].backgroundColor = UIColor.red
                    break;
                }
            }
            if logic.won {
                showMessage(text: "\(logic.previousPlayerName()) has won", actionText: "Start new game")
                newGame()
            } else if logic.draw {
                showMessage(text: "Draw", actionText: "New game")
                newGame()
            }
            setColors(available: UIColor.lightGray, none: UIColor.white)
        } else {
            showMessage(text: "This move is not allowed", actionText: "Dismiss")
        }
        logic.updateAvailableCells()
        if logic.previousPlayerName() == "Player A" {
            currentPlayerLabel.backgroundColor = UIColor.red
        } else {
            currentPlayerLabel.backgroundColor = UIColor.yellow
        }
        setColors(available: UIColor.lightGray, none: UIColor.white)
        enableButtons()
    }
    
    func clickButton(_ sender: UIButton) {
        disableButtons()
        logic.updateAvailableCells()
        let place = Board(tag: sender.tag)
        
        if logic.canStep(at: place) {
            setAvailableButtonColors(color: UIColor.white)
            switch logic.onStep(at: place) {
            case .Player_A:
                sender.backgroundColor = UIColor.yellow
            case .Player_B:
                sender.backgroundColor = UIColor.red
            default:
                break
            }
            for pos in logic.capturedCells {
                switch logic.fieldAt(pos) {
                case .None:
                    break;
                case .Player_A:
                    current_position[pos.x][pos.y].backgroundColor = UIColor.yellow
                    break;
                case .Player_B:
                    current_position[pos.x][pos.y].backgroundColor = UIColor.red
                    break;
                }
            }
            if logic.won {
                showMessage(text: "\(logic.previousPlayerName()) has won", actionText: "Start new game")
                newGame()
            } else if logic.draw {
                showMessage(text: "Draw", actionText: "New game")
                newGame()
            }
            setColors(available: UIColor.lightGray, none: UIColor.white)
        } else {
            print("Error, calculate again")
            logic.updateAvailableCells()
            makeMove(sender)
        }
        logic.updateAvailableCells()
        setColors(available: UIColor.lightGray, none: UIColor.white)
        currentPlayerLabel.backgroundColor = UIColor.yellow
        enableButtons()
    }
    
    var overlayView = UIView()
    var activityIndicator = UIActivityIndicatorView()
    
    func showOverlay(_ view: UIView!) {
        overlayView = UIView(frame: UIScreen.main.bounds)
        overlayView.backgroundColor = UIColor(red: 1, green: 1, blue: 1, alpha: 0.5)
        activityIndicator = UIActivityIndicatorView(style: UIActivityIndicatorView.Style.large)
        activityIndicator.center = overlayView.center
        overlayView.addSubview(activityIndicator)
        activityIndicator.startAnimating()
        self.view.addSubview(overlayView)
    }
    
    func hideOverlayView() {
        activityIndicator.stopAnimating()
        overlayView.removeFromSuperview()
    }
    
    func newGame() {
        logic.setCurrentPlayer(.Player_A)
        logic = ReversiLogic(width: fieldWidth, height: fieldHeight)
        clearButtons()
        generateButtonField(x: fieldWidth, y: fieldHeight, screenWidth: container.frame.width, screenHeight:  container.frame.height)
        ai = ReversiAI(logic: logic, controller: self)
    }
    
    func showMessage(text: String, actionText: String) {
        let controller : UIAlertController = UIAlertController(title: text, message: "", preferredStyle: UIAlertController.Style.alert)
        let okAction : UIAlertAction = UIAlertAction(title: actionText, style: UIAlertAction.Style.default, handler: {
            (alert: UIAlertAction!) in controller.dismiss(animated: true, completion:
                                                            nil) })
        controller.addAction(okAction)
        self.present(controller, animated: true, completion: nil)
    }
    
    func clearButtons() {
        for row in current_position {
            for button in row {
                button.removeFromSuperview()
            }
        }
        current_position.removeAll()
    }
    
    func disableButtons() {
        for buttonRow in current_position {
            for button in buttonRow {
                button.isEnabled = false
            }
        }
    }
    
    func enableButtons() {
        for buttonRow in current_position {
            for button in buttonRow {
                button.isEnabled = true
            }
        }
    }
}
