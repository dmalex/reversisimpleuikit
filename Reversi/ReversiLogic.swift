//
//  ReversiLogic.swift
//  Reversi
//
class ReversiLogic
{
    func enemyPlayer(_ player: State) -> State {
        if player == .Player_A {
            return .Player_B
        } else {
            return .Player_A
        }
    }
    
    var field: [[State]]
    private(set) var currentPlayer: State = .Player_A
    private(set) var won: Bool = false
    private(set) var draw: Bool = false
    private(set) var score1 = 2
    private(set) var score2 = 2
    private(set) var availableCells : [Board] = []
    private(set) var capturedCells : [Board] = []
    
    init(width: Int, height: Int) {
        field = Array(repeating: Array(repeating: .None, count: height), count: width)
        
        let halfWidth = width / 2
        let halfHeight = height / 2
        field[halfWidth - 1][halfHeight - 1] = .Player_A
        field[halfWidth][halfHeight] = .Player_A
        
        field[halfWidth - 1][halfHeight] = .Player_B
        field[halfWidth][halfHeight - 1] = .Player_B
        
        updateAvailableCells()
    }

    
    func getField() -> [[State]] {
        return field
    }

    
    func getScore1() -> Int{
        return score1
    }

    
    func getScore2() -> Int{
        return score2
    }

    
    func getAvailableCells() -> [Board] {
        return availableCells
    }

    
    func canStep(at: Board) -> Bool {
        if let _ = availableCells.firstIndex(of: at) {
            return true
        }
        return false
    }

    
    func canStepOnCopy(at: Board, available: [Board]) -> Bool {
        if let _ = available.firstIndex(of: at) {
            return true
        }
        return false
    }

    
    func fieldAt(_ at: Board) -> State {
        return field[at.x][at.y]
    }

    
    func fieldAt(_ at: Board, field: [[State]]) -> State {
        return field[at.x][at.y]
    }

    
    func onStep(at: Board) -> State {
        field[at.x][at.y] = currentPlayer
        
        let score = captureCells(start: at)
        if currentPlayer == .Player_A {
            score1 += 1 + score
            score2 -= score
        } else {
            score2 += 1 + score
            score1 -= score
        }
        currentPlayer = enemyPlayer(currentPlayer)
        
        if score1 + score2 == field.count * field.first!.count {
            if score1 == score2 {
                draw = true
            } else {
                won = true
            }
        } else {
            updateAvailableCells()
        }
        
        if availableCells.count == 0 {
            won = true
        }
        
        return field[at.x][at.y]
    }
    
    
    func captureCells(start: Board) -> Int {
        var sum = 0
        for dir in Board.offsets {
            sum += captureDirection(start: start, offset: dir)
        }
        return sum
    }
    
    
    func captureDirection(start: Board, offset: Board) -> Int {
        var sum = 0
        var pos = start + offset
        while isValid(pos: pos) && fieldAt(pos) == enemyPlayer(currentPlayer) {
            pos = pos + offset
        }
        if isValid(pos: pos) && fieldAt(pos) == currentPlayer {
            pos = pos - offset
            while pos != start {
                capturedCells.append(pos)
                field[pos.x][pos.y] = currentPlayer
                sum += 1
                pos = pos - offset
            }
        }
        return sum
    }
    
    
    func tryCaptureCells(start: Board, fieldCopy: [[State]], player: State) -> Int {
        var sum = 0
        for dir in Board.offsets {
            sum += tryCaptureDirection(start: start, offset: dir, fieldCopy: fieldCopy, player: player)
        }
        return sum
    }
    
    
    func tryCaptureDirection(start: Board, offset: Board, fieldCopy : [[State]], player: State) -> Int {
        var sum = 0
        var pos = start + offset
        while isValid(pos: pos) && fieldCopy[pos.x][pos.y] == enemyPlayer(player) {
            pos = pos + offset
        }
        if isValid(pos: pos) && fieldCopy[pos.x][pos.y] == player {
            pos = pos - offset
            while pos != start {
                if player == .Player_B {
                    sum += 1
                    if(borderPosition(pos)) {
                        sum += 1
                    }
                    pos = pos - offset
                }
                if player == .Player_A {
                    sum -= 1
                    if(borderPosition(pos)) {
                        sum -= 1
                    }
                    pos = pos - offset
                }
            }
        }
        return sum
    }
    
    
    func borderPosition(_ pos: Board) -> Bool {
        return pos.x == 0 || pos.y == 0 || pos.x == field.count - 1 || pos.y == field[0].count
    }
    
    
    func getCurrentPlayer() -> State {
        return currentPlayer
    }
    
    
    func previousPlayerName() -> String {
        if currentPlayer == .Player_B {
            return "Player A"
        } else {
            return "Player B"
        }
    }

    
    func updateAvailableCells() {
        availableCells = []
        for i in 0..<field.count {
            let row = field[i]
            for j in 0..<row.count {
                let pos = Board(x: i, y: j)
                if fieldAt(pos) == .None && cellIsAvailable(pos) {
                    availableCells.append(pos)
                }
            }
        }
    }

    
    func getAvailableCells(field: [[State]], player: State) -> [Board]{
        availableCells = []
        for i in 0..<field.count {
            let row = field[i]
            for j in 0..<row.count {
                let pos = Board(x: i, y: j)
                if field[pos.x][pos.y] == .None && cellIsAvailable(pos, field: field, player: player) {
                    availableCells.append(pos)
                }
            }
        }
        return availableCells
    }
    
    
    func cellIsAvailable(_ pos: Board) -> Bool {
        for dir in Board.offsets {
            if isValid(pos: pos + dir) && fieldAt(pos + dir) == enemyPlayer(currentPlayer) {
                if checkAvailableDirection(pos: pos, dir: dir) {
                    return true
                }
            }
        }
        return false
    }

    
    func cellIsAvailable(_ pos: Board, field: [[State]], player: State) -> Bool {
        for dir in Board.offsets {
            if isValid(pos: pos + dir) && fieldAt(pos + dir, field: field) == enemyPlayer(currentPlayer) {
                if checkAvailableDirection(pos: pos, dir: dir, field: field, player: player) {
                    return true
                }
            }
        }
        return false
    }

    
    func checkAvailableDirection(pos: Board, dir: Board) -> Bool {
        var curPos = pos + dir;
        while isValid(pos: curPos) {
            if fieldAt(curPos) != enemyPlayer(currentPlayer) {
                break;
            }
            curPos = curPos + dir
        }
        return isValid(pos: curPos) && fieldAt(curPos) == currentPlayer
    }

    
    func checkAvailableDirection(pos: Board, dir: Board, field: [[State]], player: State) -> Bool {
        var curPos = pos + dir;
        while isValid(pos: curPos) {
            if field[curPos.x][curPos.y] != enemyPlayer(player) {
                break;
            }
            curPos = curPos + dir
        }
        return isValid(pos: curPos) && fieldAt(curPos, field: field) == player
    }

    
    func isValid(pos: Board) -> Bool {
        return pos.x >= 0 && pos.y >= 0 && pos.x < field.count && pos.y < field[0].count
    }

    
    func setCurrentPlayer(_ player: State) {
        currentPlayer = player
    }
}
